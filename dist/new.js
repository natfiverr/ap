
// MORMING AND AFTERNOON APPTS POSSIBILLITIES
let morningPossibilites = [];
let afterNonnPossibilites = [];

let morningPossibilitesFinal = [];
let afterNonnPossibilitesFinal = [];

let randomMorning = [];
let randomAfternoon = [];

let data;

let excludeDaysArr;

const event = "FALSE";

// EVENT LISTENER
const form = document.querySelector("#form");


form.addEventListener('submit', function(e){ 
    document.querySelector("#book-list").innerHTML = '';

    morningPossibilites = [];
    afterNonnPossibilites = [];

    morningPossibilitesFinal = [];
    afterNonnPossibilitesFinal = [];

    randomMorning = [];
    randomAfternoon = [];

    excludeDaysArr = [];

    let max_noon_before = parseInt(document.querySelector("#max_noon_before").value);
    let max_noon_after = parseInt(document.querySelector("#max_noon_after").value);

    const start_date = document.querySelector("#start_date").value;
    const end_date = document.querySelector("#end_date").value;


    let min_time = document.querySelector("#min_time").value;    
    let max_time = document.querySelector("#max_time").value;

    min_time = parseInt(min_time);
    max_time = parseInt(max_time);

    let dur = document.querySelector("#min_dur").value;
    dur = parseInt(dur);

        // GETTING DATES BETWEEN THE START AND END DATE
    const new_start_date = new Date(start_date);
    const new_end_date = new Date(end_date);
    data = getDates(new_start_date, new_end_date);

      // EXLUDE DAYS DATA
      const excludeDates = document.querySelector(".excludeDates").value;
        excludeDaysArr = excludeDates.split(",");


let mornStrt;
let aftStrt;
    
let i;

if(min_time == 24){
    min_time = 1;
    morningPossibilites.push(12);
    morningPossibilites.push(12.15);
    morningPossibilites.push(12.30);
    morningPossibilites.push(12.45);
}
if(min_time < 12){
    if(max_time > 12){
        mornStrt = 12;
    }else{
        mornStrt = max_time;
    }
    for(i = min_time ; i < mornStrt; i+=.15){
    if(i == 13){
        i = 1;
    }
    if((i % 1) > 0.5){
        i = Math.round(i); 
        i = round(i, 3);
        morningPossibilites.push(i);
    }else{ 
        i = round(i, 3);
        morningPossibilites.push(i);
    }
    }

}
if(max_time > 11){
    morningPossibilites.pop();
}


console.log(min_time, max_time);

let z;
// afterNonnPossibilites.push(12);
// afterNonnPossibilites.push(12.15);
// afterNonnPossibilites.push(12.30);
// afterNonnPossibilites.push(12.45);


if(max_time > 12){
    if(min_time < 12){
        aftStrt = 12;
    }else{
        aftStrt = min_time;
    }


    for(z = aftStrt ; z < max_time; z+=.15){
        if((z % 1) > 0.5){
            z = Math.round(z); 
            z = round(z, 3);
            afterNonnPossibilites.push(z);
        }else{ 
            z = round(z, 3);
            afterNonnPossibilites.push(z);
        }
    }
    
}
if(max_time == 24){
    afterNonnPossibilites.pop();
}

if(dur == 30){
    morningPossibilites = morningPossibilites.filter((_,i) => i % 2 == 0); 
    afterNonnPossibilites = afterNonnPossibilites.filter((_,i) => i % 2 == 0); 
}else if(dur == 45){
    morningPossibilites = morningPossibilites.filter((_,i) => i % 3 == 0); 
    afterNonnPossibilites = afterNonnPossibilites.filter((_,i) => i % 3 == 0); 
}else if(dur == 60){
    morningPossibilites = morningPossibilites.filter((_,i) => i % 4 == 0); 
    afterNonnPossibilites = afterNonnPossibilites.filter((_,i) => i % 4 == 0); 
}else if(dur == 90){
    morningPossibilites = morningPossibilites.filter((_,i) => i % 6 == 0); 
    afterNonnPossibilites = afterNonnPossibilites.filter((_,i) => i % 6 == 0); 
}else if(dur == 120){
    morningPossibilites = morningPossibilites.filter((_,i) => i % 8 == 0); 
    afterNonnPossibilites = afterNonnPossibilites.filter((_,i) => i % 8 == 0); 
}else if(dur == 150){
    morningPossibilites = morningPossibilites.filter((_,i) => i % 10 == 0); 
    afterNonnPossibilites = afterNonnPossibilites.filter((_,i) => i % 10 == 0); 
}

morningPossibilites.forEach(function(item, index) {
        const val = {"start":item, "end":morningPossibilites[index + 1]};
        morningPossibilitesFinal.push(val);
});

afterNonnPossibilites.forEach(function(item, index) {
        const val = {"start":item, "end":afterNonnPossibilites[index + 1]};
        afterNonnPossibilitesFinal.push(val);
});
morningPossibilitesFinal.pop();
afterNonnPossibilitesFinal.pop();



morningPossibilitesFinal.forEach(function(number,index){
    if((number.end % 1) > 0.5){
        this[index].end = Math.round(number.end);
    }
},morningPossibilitesFinal)

morningPossibilitesFinal.forEach(function(number,index){
    if(Number.isInteger(number.start)){
        let num = number.start + ":00";
        this[index].start = num;
    }
    if(Number.isInteger(number.end)){
        let num = number.end + ":00";
        this[index].end = num;
    }
    if(getDecimal(number.end) == 0.30000000000000004 || getDecimal(number.end) == 0.2999999999999998 || getDecimal(number.end) == 0.3000000000000007){
        let num = round(number.end, 2) + "0";
        this[index].end = num;
    }
    if(getDecimal(number.start) == 0.30000000000000004 || getDecimal(number.start) == 0.2999999999999998 || getDecimal(number.start) == 0.3000000000000007){
        let num = round(number.start, 2) + "0";
        this[index].start = num;
    }
    this[index].start = number.start.toString();
    this[index].end = number.end.toString();

    this[index].start = this[index].start.replace(/\./g,':');
    this[index].end = this[index].end.replace(/\./g,':');

},morningPossibilitesFinal)

afterNonnPossibilitesFinal.forEach(function(number,index){
    if((number.end % 1) > 0.5){
        this[index].end = Math.round(number.end);
    }
},afterNonnPossibilitesFinal)

afterNonnPossibilitesFinal.forEach(function(number,index){
    if(Number.isInteger(number.start)){
        let num2 = number.start + ":00";
        this[index].start = num2;
    }
    if(Number.isInteger(number.end)){
        let num2 = number.end + ":00";
        this[index].end = num2;
    }
    if(getDecimal(number.end) == 0.30000000000000004 || getDecimal(number.end) == 0.2999999999999998 || getDecimal(number.end) == 0.3000000000000007){
        let num2 = round(number.end, 2) + "0";
        this[index].end = num2;
    }
    if(getDecimal(number.start) == 0.30000000000000004 || getDecimal(number.start) == 0.2999999999999998 || getDecimal(number.start) == 0.3000000000000007) {
        let num2 = round(number.start, 2) + "0";
        this[index].start = num2;
    }
    this[index].start = number.start.toString();
    this[index].end = number.end.toString();

    this[index].start = this[index].start.replace(/\./g,':');
    this[index].end = this[index].end.replace(/\./g,':');

},afterNonnPossibilitesFinal)


console.log(morningPossibilitesFinal);

console.log(afterNonnPossibilitesFinal);

if(morningPossibilitesFinal.length > max_noon_before){
    max_noon_before = max_noon_before;
}else if(morningPossibilitesFinal.length == 0){
    max_noon_before = 0;
}else{
    max_noon_before = morningPossibilitesFinal.length;
} 


if(afterNonnPossibilitesFinal.length > max_noon_after){
    max_noon_after = max_noon_after;
}else if(afterNonnPossibilitesFinal.length == 0){
    max_noon_after = 0;
}else{
    max_noon_after = afterNonnPossibilitesFinal.length;
}  



validateFields(start_date, end_date,max_noon_after, max_noon_before, min_time,max_time,dur);


 e.preventDefault();   
});


function validateFields(start_date, end_date,max_noon_after, max_noon_before, min_time,max_time,dur){
    if(start_date > end_date){
        alert('ERROR! You Entered The End Date Earlier Than The Start Date, please generate Appts Again');
    }
    else if(max_noon_before == '0' && max_noon_after == '0'){
        alert("0 Appts Generated");
    }
    else{
        generateAppts(start_date, end_date,max_noon_after, max_noon_before, min_time,max_time,dur);
        clearFields();
    }

}

function generateAppts(start_date, end_date,max_noon_after, max_noon_before, min_time,max_time,dur){
    if(document.getElementById("is_weekend").checked == true && document.getElementById("exclude").checked == false){
        console.log("include weekend and no exclude");
        data.forEach(function(data){

            const date_start = data.toISOString().slice(0,10);
                
            randomMorning = getRandom(morningPossibilitesFinal, max_noon_before);
            randomAfternoon = getRandom(afterNonnPossibilitesFinal, max_noon_after);

            randomMorning.forEach(pbt => {
                // GENERATING VALUES
                let strtTime = pbt.start;
                let endTime = pbt.end;


                const list = document.querySelector("#book-list");
                const row = document.createElement('tr');
                row.innerHTML = `
                <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                <td>${date_start}</td>
                <td>${strtTime} AM</td>
                <td>${date_start}</td>
                <td>${endTime} AM</td>
                <td>${event}</td>
                <td>test description</td>`;
                list.appendChild(row);
        });

        randomAfternoon.forEach(pbt => {
            // GENERATING VALUES
            let strtTime = pbt.start;
            let endTime = pbt.end;


            const list = document.querySelector("#book-list");
            const row = document.createElement('tr');
            row.innerHTML = `
            <td>FAKE ${date_start}/${strtTime} ${dur}</td>
            <td>${date_start}</td>
            <td>${strtTime} PM</td>
            <td>${date_start}</td>
            <td>${endTime} PM</td>
            <td>${event}</td>
            <td>test description</td>`;
            list.appendChild(row);
        });

        });
        
    }// END IF
    
    
    else if(document.getElementById("is_weekend").checked == false && document.getElementById("exclude").checked == true){
        console.log("exclude weekend and include exclude");
        data.forEach(function(data){
            const date_start = data.toISOString().slice(0,10);

            randomMorning = getRandom(morningPossibilitesFinal, max_noon_before);
            randomAfternoon = getRandom(afterNonnPossibilitesFinal, max_noon_after);

            if(!excludeDaysArr.includes(date_start)){
            if(data.getDay() != 5 && data.getDay() != 6){

                randomMorning = getRandom(morningPossibilitesFinal, max_noon_before);
                randomAfternoon = getRandom(afterNonnPossibilitesFinal, max_noon_after);

                randomMorning.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;


                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime} AM</td>
                    <td>${date_start}</td>
                    <td>${endTime} AM</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });

            randomAfternoon.forEach(pbt => {
                // GENERATING VALUES
                let strtTime = pbt.start;
                let endTime = pbt.end;


                const list = document.querySelector("#book-list");
                const row = document.createElement('tr');
                row.innerHTML = `
                <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                <td>${date_start}</td>
                <td>${strtTime} PM</td>
                <td>${date_start}</td>
                <td>${endTime} PM</td>
                <td>${event}</td>
                <td>test description</td>`;
                list.appendChild(row);
            });
                
            }
        }
        });

    }// END ELSE IF
    
    else if(document.getElementById("is_weekend").checked == true && document.getElementById("exclude").checked == true){
        console.log("include weekend and include exclude");
        data.forEach(function(data){
             
            const date_start = data.toISOString().slice(0,10);

            randomMorning = getRandom(morningPossibilitesFinal, max_noon_before);
            randomAfternoon = getRandom(afterNonnPossibilitesFinal, max_noon_after);

            if(!excludeDaysArr.includes(date_start)){
                
                randomMorning = getRandom(morningPossibilitesFinal, max_noon_before);
                randomAfternoon = getRandom(afterNonnPossibilitesFinal, max_noon_after);

                randomMorning.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;


                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime} AM</td>
                    <td>${date_start}</td>
                    <td>${endTime} AM</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });

            randomAfternoon.forEach(pbt => {
                // GENERATING VALUES
                let strtTime = pbt.start;
                let endTime = pbt.end;


                const list = document.querySelector("#book-list");
                const row = document.createElement('tr');
                row.innerHTML = `
                <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                <td>${date_start}</td>
                <td>${strtTime} PM</td>
                <td>${date_start}</td>
                <td>${endTime} PM</td>
                <td>${event}</td>
                <td>test description</td>`;
                list.appendChild(row);
            });
            }
        });
    }// END ELSE IF
    
    else if(document.getElementById("is_weekend").checked == false && document.getElementById("exclude").checked == false){
        console.log("exclude weekends and no exclude");
        data.forEach(function(data){
             
            const date_start = data.toISOString().slice(0,10);

            if(data.getDay() != 5 && data.getDay() != 6){

                randomMorning = getRandom(morningPossibilitesFinal, max_noon_before);
                randomAfternoon = getRandom(afterNonnPossibilitesFinal, max_noon_after);

                randomMorning.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;


                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime} AM</td>
                    <td>${date_start}</td>
                    <td>${endTime} AM</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });

            randomAfternoon.forEach(pbt => {
                // GENERATING VALUES
                let strtTime = pbt.start;
                let endTime = pbt.end;


                const list = document.querySelector("#book-list");
                const row = document.createElement('tr');
                row.innerHTML = `
                <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                <td>${date_start}</td>
                <td>${strtTime} PM</td>
                <td>${date_start}</td>
                <td>${endTime} PM</td>
                <td>${event}</td>
                <td>test description</td>`;
                list.appendChild(row);
            });
    

            }
        });
    }// END ELSE IF
}

function clearFields(){
    if(document.querySelector("#btnExportToCsv").disabled == true){
        document.querySelector("#btnExportToCsv").disabled = false;
        document.querySelector("#btnExportToCsv").classList.remove('tooltip');
        document.querySelector(".tooltiptext").remove();
    }
}