
let morningPossibilites = [];
let afterNonnPossibilites = [];

let morningPossibilitesFinal = [];
let afterNonnPossibilitesFinal = [];


// EVENT LISTENER
const form = document.querySelector("#form");
form.addEventListener('submit', function(e){ 
    const max_noon_before = document.querySelector("#max_noon_before").value;
    const max_noon_after = document.querySelector("#max_noon_after").value;

    const start_date = document.querySelector("#start_date").value;
    const end_date = document.querySelector("#end_date").value;

    // EXLUDE DAYS DATA
    const excludeDates = document.querySelector(".excludeDates").value;
    const excludeDaysArr = excludeDates.split(",");


    const event = 'FALSE';

    const dur = document.querySelector("#min_dur").value;

    let min_time = document.querySelector("#min_time").value;
    min_time = parseInt(min_time.replace(/[0:PMAM]/g, ""));
    
    let max_time = document.querySelector("#max_time").value;
    
    max_time = parseInt(max_time.replace(/[0:PMAM]/g, ""));


    if(min_time == 1){
        min_time = 10;
    }

    // FOR MORNING
    if(min_time == 9 && dur == 15){
        morningPossibilites = [
            {start: '9:00 AM', end:'9:15 AM'},
            {start: '9:15 AM', end:'9:30 AM'},
            {start: '9:30 AM', end:'9:45 AM'},
            {start: '9:45 AM', end:'10:00 AM'},
            {start: '10:00 AM', end:'10:15 AM'},
            {start: '10:15 AM', end:'10:30 AM'},
            {start: '10:30 AM', end:'10:45 AM'},
            {start: '10:45 AM', end:'11:00 AM'},
            {start: '11:00 AM', end:'11:15 AM'},
            {start: '11:15 AM', end:'11:30 AM'},
            {start: '11:30 AM', end:'11:45 AM'},
        ]
    }else if(min_time == 10 && dur == 15){
        morningPossibilites = [
            {start: '10:00 AM', end:'10:15 AM'},
            {start: '10:15 AM', end:'10:30 AM'},
            {start: '10:30 AM', end:'10:45 AM'},
            {start: '10:45 AM', end:'11:00 AM'},
            {start: '11:00 AM', end:'11:15 AM'},
            {start: '11:15 AM', end:'11:30 AM'},
            {start: '11:30 AM', end:'11:45 AM'},
            
        ]
    }else if(min_time == 11 && dur == 15){
        morningPossibilites = [
            {start: '11:00 AM', end:'11:15 AM'},
            {start: '11:15 AM', end:'11:30 AM'},
            {start: '11:30 AM', end:'11:45 AM'},
        ]
    }else if(min_time == 9 && dur == 30){
        morningPossibilites = [
            {start: '9:00 AM', end:'9:30 AM'},
            {start: '9:30 AM', end:'10:00 AM'},
            {start: '10:00 AM', end:'10:30 AM'},
            {start: '10:30 AM', end:'11:00 AM'},
            {start: '11:00 AM', end:'11:30 AM'},
        ]
    }else if(min_time == 10 && dur == 30){
        morningPossibilites = [
            {start: '10:00 AM', end:'10:30 AM'},
            {start: '10:30 AM', end:'11:00 AM'},
            {start: '11:00 AM', end:'11:30 AM'},
        ]
    }else if(min_time == 11 && dur == 30){
        morningPossibilites = [
            {start: '11:00 AM', end:'11:30 AM'},
        ]
    }

    // DURATION 45 MIN
    else if(min_time == 9 && dur == 45){
        morningPossibilites = [
            {start: '9:00 AM', end:'9:45 AM'},
            {start: '9:45 AM', end:'10:30 AM'},
            {start: '10:30 AM', end:'11:00 AM'},
            {start: '11:00 AM', end:'11:45 AM'},
        ]
    }else if(min_time == 10 && dur == 45){
        morningPossibilites = [
            {start: '10:00 AM', end:'10:45 AM'},
            {start: '10:45 AM', end:'11:30 AM'},
        ]
    }else if(min_time == 11 && dur == 45){
        morningPossibilites = [
            {start: '11:00 AM', end:'11:45 AM'},
        ]
    }

    // DURATION 60 MIN

    else if(min_time == 9 && dur == 60){
        morningPossibilites = [
            {start: '9:00 AM', end:'10:00 AM'},
            {start: '10:00 AM', end:'11:00 AM'},
        ]
    }else if(min_time == 10 && dur == 60){
        morningPossibilites = [
            {start: '10:00 AM', end:'11:00 AM'},
        ]
    }else if(min_time == 11 && dur == 60){
        morningPossibilites = []
    }

    // DURATION 90 MIN
    
    else if(min_time == 9 && dur == 90){
        morningPossibilites = [
            {start: '9:00 AM', end:'10:30 AM'},
        ]
    }else if(min_time == 10 && dur == 90){
        morningPossibilites = [
            {start: '10:00 AM', end:'11:30 AM'},
        ]
    }else if(min_time == 11 && dur == 90){
        morningPossibilites = [];
    }

    // DURATION 120 MIN

      else if(min_time == 9 && dur == 120){
        morningPossibilites = [
            {start: '9:00 AM', end:'11:00 AM'},
        ]
    }else if(min_time == 10 && dur == 120){
        morningPossibilites = []
    }else if(min_time == 11 && dur == 120){
        morningPossibilites = [];
    }

     // DURATION 150 MIN

     else if(min_time == 9 && dur == 150){
        morningPossibilites = [
            {start: '9:00 AM', end:'11:30 AM'},
        ]
    }else if(min_time == 10 && dur == 150){
        morningPossibilites = [];

    }else if(min_time == 11 && dur == 150){
        morningPossibilites = [];
    }


    // IF MIN TIME SELECTED IS GREATED THAN 12 PM, THEN NO MORNING APPTS
    else if(min_time >= 12){
        morningPossibilites = [];
    }


    // FOR AFTER NOON
    if(max_time == 17 && dur == 15){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:15 PM'},
            {start: '12:15 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:15 PM'},
            {start: '1:15 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'1:45 PM'},
            {start: '1:45 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'2:15 PM'},
            {start: '2:15 PM', end:'2:30 PM'},
            {start: '2:30 PM', end:'2:45 PM'},
            {start: '2:45 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'3:15 PM'},
            {start: '3:15 PM', end:'3:30 PM'},
            {start: '3:30 PM', end:'3:45 PM'},
            {start: '3:45 PM', end:'4:00 PM'},
            {start: '4:00 PM', end:'4:15 PM'},
            {start: '4:15 PM', end:'4:30 PM'},
            {start: '4:30 PM', end:'4:45 PM'},
            {start: '4:45 PM', end:'5:00 PM'},
        ]
    }else if(max_time == 16 && dur == 15){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:15 PM'},
            {start: '12:15 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:15 PM'},
            {start: '1:15 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'1:45 PM'},
            {start: '1:45 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'2:15 PM'},
            {start: '2:15 PM', end:'2:30 PM'},
            {start: '2:30 PM', end:'2:45 PM'},
            {start: '2:45 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'3:15 PM'},
            {start: '3:15 PM', end:'3:30 PM'},
            {start: '3:30 PM', end:'3:45 PM'},
            {start: '3:45 PM', end:'4:00 PM'},
        ]
    }else if(max_time == 15 && dur == 15){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:15 PM'},
            {start: '12:15 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:15 PM'},
            {start: '1:15 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'1:45 PM'},
            {start: '1:45 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'2:15 PM'},
            {start: '2:15 PM', end:'2:30 PM'},
            {start: '2:30 PM', end:'2:45 PM'},
            {start: '2:45 PM', end:'3:00 PM'},
        ]
    }else if(max_time == 14 && dur == 15){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:15 PM'},
            {start: '12:15 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:15 PM'},
            {start: '1:15 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'1:45 PM'},
            {start: '1:45 PM', end:'2:00 PM'},
        ]
    }else if(max_time == 13 && dur == 15){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:15 PM'},
            {start: '12:15 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:00 PM'},
        ]
    }
    
    // DURATION 30 MIN
    else if(max_time == 17 && dur == 30){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'2:30 PM'},
            {start: '2:30 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'3:30 PM'},
            {start: '3:30 PM', end:'4:00 PM'},
            {start: '4:00 PM', end:'4:30 PM'},
            {start: '4:30 PM', end:'5:00 PM'},
        ]
    }else if(max_time == 16 && dur == 30){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'2:30 PM'},
            {start: '2:30 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'3:30 PM'},
            {start: '3:30 PM', end:'4:00 PM'},
        ]
    }else if(max_time == 15 && dur == 30){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'2:30 PM'},
            {start: '2:30 PM', end:'3:00 PM'},
        ]
    }else if(max_time == 14 && dur == 30){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'2:00 PM'},
        ]
    }else if(max_time == 13 && dur == 30){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:30 PM'},
            {start: '12:30 PM', end:'1:00 PM'},
        ]
    }

    // DURATION 45 MIN
    if(max_time == 17 && dur == 45){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'2:15 PM'},
            {start: '2:15 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'3:45 PM'},
            {start: '3:45 PM', end:'4:30 PM'},
        ]
    }else if(max_time == 16 && dur == 45){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'2:15 PM'},
            {start: '2:15 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'3:45 PM'},

        ]
    }else if(max_time == 15 && dur == 45){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'2:15 PM'},
            {start: '2:15 PM', end:'3:00 PM'},
        ]
    }else if(max_time == 14 && dur == 45){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:45 PM'},
            {start: '12:45 PM', end:'1:30 PM'},
        ]
    }else if(max_time == 13 && dur == 45){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'12:45 PM'},
        ]
    }

    // DURATION 60 MIN

    if(max_time == 17 && dur == 60){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'4:00 PM'},
            {start: '4:00 PM', end:'5:00 PM'},
        ]
    }else if(max_time == 16 && dur == 60){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'4:00 PM'},

        ]
    }else if(max_time == 15 && dur == 60){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'3:00 PM'},
        ]
    }else if(max_time == 14 && dur == 60){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:00 PM'},
            {start: '1:00 PM', end:'2:00 PM'},
        ]
    }else if(max_time == 13 && dur == 60){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:00 PM'},
        ]
    }

    
    // DURATION 90 MIN

    if(max_time == 17 && dur == 90){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'3:00 PM'},
            {start: '3:00 PM', end:'4:30 PM'},
        ]
    }else if(max_time == 16 && dur == 90){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'3:00 PM'},

        ]
    }else if(max_time == 15 && dur == 90){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:30 PM'},
            {start: '1:30 PM', end:'3:00 PM'},
        ]
    }else if(max_time == 14 && dur == 90){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'1:30 PM'},
        ]
    }else if(max_time == 13 && dur == 90){
        afterNonnPossibilites = [];
    }


      
    // DURATION 120 MIN

    if(max_time == 17 && dur == 120){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'4:00 PM'},
        ]
    }else if(max_time == 16 && dur == 120){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'2:00 PM'},
            {start: '2:00 PM', end:'4:00 PM'},

        ]
    }else if(max_time == 15 && dur == 120){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'2:00 PM'},
        ]
    }else if(max_time == 14 && dur == 120){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'2:00 PM'},
        ]
    }else if(max_time == 13 && dur == 120){
        afterNonnPossibilites = [];
    }

       
    // DURATION 150 MIN

    if(max_time == 17 && dur == 150){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'2:30 PM'},
            {start: '2:30 PM', end:'5:00 PM'},
        ]
    }else if(max_time == 16 && dur == 150){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'2:30 PM'},

        ]
    }else if(max_time == 15 && dur == 150){
        afterNonnPossibilites = [
            {start: '12:00 PM', end:'2:30 PM'},
        ]
    }else if(max_time == 14 && dur == 150){
        afterNonnPossibilites = [];

    }else if(max_time == 13 && dur == 150){
        afterNonnPossibilites = [];

    }


    else if(max_time > 17 ){
        afterNonnPossibilites = [];
    }

    

    morningPossibilitesFinal = morningPossibilites;
    afterNonnPossibilitesFinal = afterNonnPossibilites;


    
    const new_start_date = new Date(start_date);

    const new_end_date = new Date(end_date);

    const data = getDates(new_start_date, new_end_date);




    if(document.getElementById("is_weekend").checked == true && document.getElementById("exclude").checked == false){
        if(validateFields(start_date, end_date,max_noon_after, max_noon_before,min_time,max_time)){
        console.log('none');
        data.forEach(function(data){
            // DATE
            const date_start = data.toISOString().slice(0,10);
            if(max_noon_before == '0' || max_noon_before == ''){
                morningPossibilitesFinal = getRandom(morningPossibilites, 0);
            }else{
                morningPossibilitesFinal = getRandom(morningPossibilites, max_noon_before);
            }            


            morningPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;


                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
                    
            });
            morningPossibilitesFinal = [];


            if(max_noon_after == '0' || max_noon_after == ''){
                afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, 0);
            }else{
                afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, max_noon_after);
            }            


            afterNonnPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;

                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });
            afterNonnPossibilitesFinal = [];        
                
        })
    }// end valdidate
    }else if(document.getElementById("exclude").checked == true && document.getElementById("is_weekend").checked == false){
        if(validateFields(start_date, end_date,max_noon_after, max_noon_before,min_time,max_time)){
        
        console.log('weekend and exclude');
        data.forEach(function(data){
            let dates = data.toISOString().slice(0,10);
            dates = changeDateFormat(dates);

            if(data.getDay() != 5 && data.getDay() != 6){

            if(!excludeDaysArr.includes(dates)){
                 // DATE
            const date_start = data.toISOString().slice(0,10);
            if(max_noon_before == '0' || max_noon_before == ''){
                morningPossibilitesFinal = getRandom(morningPossibilites, 0);
            }else{
                morningPossibilitesFinal = getRandom(morningPossibilites, max_noon_before);
            }            


            morningPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;


                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
                    
            });
            morningPossibilitesFinal = [];


            if(max_noon_after == '0' || max_noon_after == ''){
                afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, 0);
            }else{
                afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, max_noon_after);
            }            


            afterNonnPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;

                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });
            afterNonnPossibilitesFinal = [];     
            }
        }

        })
    }
    }else if(document.getElementById("exclude").checked == true && document.getElementById("is_weekend").checked == true){
        if(validateFields(start_date, end_date,max_noon_after, max_noon_before,min_time,max_time)){
        data.forEach(function(data){
            let dates = data.toISOString().slice(0,10);
            dates = changeDateFormat(dates);
            if(!excludeDaysArr.includes(dates)){
                 // DATE
            const date_start = data.toISOString().slice(0,10);
            if(max_noon_before == '0' || max_noon_before == ''){
                morningPossibilitesFinal = getRandom(morningPossibilites, 0);
            }else{
                morningPossibilitesFinal = getRandom(morningPossibilites, max_noon_before);
            }            


            morningPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;


                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
                    
            });
            morningPossibilitesFinal = [];


            if(max_noon_after == '0' || max_noon_after == ''){
                afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, 0);
            }else{
                afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, max_noon_after);
            }            


            afterNonnPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;

                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });
            afterNonnPossibilitesFinal = [];     
            }
        })
    }
    }
    
    else{
        if(validateFields(start_date, end_date,max_noon_after, max_noon_before,min_time,max_time) == true){
        console.log('weekend');
        data.forEach(function(data){
            if(data.getDay() != 5 && data.getDay() != 6){
                 const date_start = data.toISOString().slice(0,10);
                
                if(max_noon_before == '0' || max_noon_before == ''){
                    morningPossibilitesFinal = getRandom(morningPossibilites, 0);
                }else{
                    morningPossibilitesFinal = getRandom(morningPossibilites, max_noon_before);
                }

                morningPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;


                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });


                if(max_noon_after == '0' || max_noon_after == ''){
                    afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, 0);
                }else{
                    afterNonnPossibilitesFinal = getRandom(afterNonnPossibilites, max_noon_after);
                }


                // FOR AFTER NOON
                afterNonnPossibilitesFinal.forEach(pbt => {
                    // GENERATING VALUES
                    let strtTime = pbt.start;
                    let endTime = pbt.end;

                    const list = document.querySelector("#book-list");
                    const row = document.createElement('tr');
                    row.innerHTML = `
                    <td>FAKE ${date_start}/${strtTime} ${dur}</td>
                    <td>${date_start}</td>
                    <td>${strtTime}</td>
                    <td>${date_start}</td>
                    <td>${endTime}</td>
                    <td>${event}</td>
                    <td>test description</td>`;
                    list.appendChild(row);
            });
            

            }
        })
    }// end valdidate

    }

    e.preventDefault(); 

    clearFields();

 })

 
 
 





Date.prototype.addDays = function(days) {
    var dat = new Date(this.valueOf())
    dat.setDate(dat.getDate() + days);
    return dat;
}

function getDates(startDate, stopDate) {
   var dateArray = new Array();
   var currentDate = startDate;
   while (currentDate <= stopDate) {
     dateArray.push(currentDate)
     currentDate = currentDate.addDays(1);
   }
   return dateArray;
 }



 // GENERATE RANDOM NUMBER
 function getRndInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1) ) + min;
  }


function calculateDuration(min, max){
    var duration = getRndInteger(min,max);
    return duration;
}



function tConvert (time) {
  // Check correct time format and split into components
  time = time.toString().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

  if (time.length > 1) { // If time format correct
    time = time.slice (1);  // Remove full string match value
    time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
    time[0] = +time[0] % 12 || 12; // Adjust hours
  }
  return time.join (''); // return adjusted time or original string
}








// CLEAR FIELDS
function clearFields(){
    document.querySelector("#is_weekend").checked = false;
    document.querySelector("#exclude").checked = false;
    document.querySelector("#start_date").value = '';
    document.querySelector("#end_date").value = '';
    document.querySelector("#max_noon_before").value = document.querySelector("#max_noon_before").getAttribute('data');
    document.querySelector("#max_noon_after").value = document.querySelector("#max_noon_after").getAttribute('data');
    document.querySelector("#min_time").value = document.querySelector("#min_time").getAttribute('data');
    document.querySelector("#max_time").value = document.querySelector("#max_time").getAttribute('data');
    document.querySelector("#min_dur").value = document.querySelector("#min_dur").getAttribute('data');
    document.querySelector(".excludeDates").value = '';
    document.querySelector(".excludeCalendar").style.display = 'none';
}



function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}



function validateFields(start_date, end_date,max_noon_after, max_noon_before, min_time,max_time){
    if(start_date > end_date){
        alert('ERROR! You Entered The End Date Earlier Than The Start Date, please generate Appts Again');
        return false;
    }else if(min_time < 9 || Number.isNaN(min_time)){
        alert('ERROR! Morning Time Should be Between 9:00 AM and 11:00 AM');
        return false;
    }else if(max_time < 13 || Number.isNaN(min_time) || max_time > 17){
        alert('ERROR! Afternoon Time Should be Between 1:00 PM and 5:00 PM');
        return false;
    }
    else if(max_noon_before == '0' && max_noon_after == '0'){
        alert("0 Appts Generated");
        return false;
    }
    else if(max_noon_before == '' && max_noon_after == ''){
        alert("0 Appts Generated");
        return false;
    }
    else if(max_noon_before > morningPossibilites.length){
        alert('Error, you have entered greater number of max morning Appts than possible');
        return false;
    }else if(max_noon_after > afterNonnPossibilites.length){
        alert('Error, you have entered greater number of max afternoon Appts than possible');
        return false;
    }else{
        if(document.querySelector("#btnExportToCsv").disabled == true){
            document.querySelector("#btnExportToCsv").disabled = false;
            document.querySelector("#btnExportToCsv").classList.remove('tooltip');
            document.querySelector(".tooltiptext").remove();
            document.querySelector("#is_weekend").checked = false;
            document.querySelector("#exclude").checked = false;
            document.querySelector(".excludeDates").value = '';
            document.querySelector(".excludeCalendar").style.display = 'none';
        }
        return true;
    }

}


function changeDateFormat(inputDate){  // expects Y-m-d
    var splitDate = inputDate.split('-');
    if(splitDate.count == 0){
        return null;
    }

    var year = splitDate[0];
    var month = splitDate[1];
    var day = splitDate[2]; 

    return day + '-' + month + '-' + year;
}